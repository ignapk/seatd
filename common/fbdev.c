#include <string.h>

#include "fbdev.h"

#define STRLEN(s)                 ((sizeof(s) / sizeof(s[0])) - 1)
#define STR_HAS_PREFIX(prefix, s) (strncmp(prefix, s, STRLEN(prefix)) == 0)

#if defined(__linux__)
int path_is_fbdev(const char *path) {
	if (STR_HAS_PREFIX("/dev/fb", path))
		return 1;
	return 0;
}
#elif defined(__FreeBSD__) || defined(__NetBSD__)
int path_is_fbdev(const char *path) {
	(void)path;
	return 0;
}
#else
#error Unsupported platform
#endif
